function common(){

}

/**
 * 删除数组内的数值
 * @param {*} num 需要删除的数字
 * @param {*} arr 原数组
 * @return 返回删除数字后的数组
 */
common.prototype.deleteNumInArr = function (num, arr) {
  let index = arr.indexOf(num);
  if (index !== -1){
    arr.splice(index, 1);
    return arr
  }else{
    console.error("数组内不存在该数值")
  }
}

module.exports = function(){
  return new common();
}