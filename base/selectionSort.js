// 选择排序
// 依次拿数组内的数字与后面的数据做比较
let arr = [3,1,2,9,5,7,6,4,8];

// 如 3 和 [1,2,9,5,7,6,4,8]
// 第一个数字3比较一圈发现 1 是数组内最小的
// 于是 3 和 1 交换位置
// 第一轮比较结束 let arr = [1,3,2,9,5,7,6,4,8]
// 第二轮比较开始 3 和 [2,9,5,7,6,4,8]比较
// 同样比较一圈发现 2 是最小的
// 交换位置 let arr = [1,2,3,9,5,7,6,4,8]
// 之后的的依次类推
function selectSort(arr) {
  let len = arr.length;
  let minIndex, temp;
  for (let i = 0; i < arr.length; i++) {
    minIndex = i;
    for (let j = i + 1; j < len; j++) {
      if (arr[j] < arr[minIndex]) { // 一一和设置的最小值索引比较
        minIndex = j                // 得到数组内最小值索引
      }
    }
    temp = arr[i]                   // 缓存第一层遍历的值
    arr[i] = arr[minIndex]          // 替换第一层遍历的值为最小值
    arr[minIndex] = temp            // 替换最小值位置为第一层遍历的值
  }
  return arr
}

console.log(selectSort(arr));