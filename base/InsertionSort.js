// 插入排序
// 依次拿数组内的数字与前面的数字做比较
// let arr = [3,1,2,9,5,7,6,4,8]
// 从第二位开始 1 与 3 比较 1小于3, 进入while
function insertionSort(arr) {
  var len = arr.length; //9
  var preIndex, current;
  for (var i = 1; i < len; i++) {
    preIndex = i - 1;  //0 1
    current = arr[i];  //1 2
    while (preIndex >= 0 && arr[preIndex] > current) {
      arr[preIndex + 1] = arr[preIndex];
      // let arr = [3,3,2,9,5,7,6,4,8]
      // let arr = [1,3,3,9,5,7,6,4,8]
      preIndex--; // -1 0
    }
    arr[preIndex + 1] = current;
    // let arr = [1,3,2,9,5,7,6,4,8]
    // let arr = [1,2,3,9,5,7,6,4,8]
  }
  return arr;
}